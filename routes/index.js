var express = require('express');
var router = express.Router();
const LRU = require('../controllers/lru')
const lru = new LRU(4)  // size initialize
/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'Express' });
});

router.get('/value',lru.get)
router.get('/values',lru.getAll)


module.exports = router;
