
class LRU {
    constructor(n = 10) {
        this.n = n;
        this.data = new Map();
        this.get = this.get.bind(this);
        this.getAll = this.getAll.bind(this);

     }
    get (request,response) {
        let key1 = request.query.key;
        let item = this.data.get(key1);
        if (item) {
            // set/update the value
            let val = parseInt(item) + 1
            this.data.set(key1,val);
        } else {
            // else set in map
            if (this.data.size == this.n) {
                // remove least used
                let least = Number.MAX_SAFE_INTEGER;
                let lastKey = ''
                for (let [key,value] of this.data.entries()) {
                    if (value < least) {
                        lastKey = key;
                        least = value
                    }
                }
                this.data.delete(lastKey)
            }
            this.data.set(key1,1);
        }
        return response.send({key: key1})

    }

    getAll(request,response) {
        let list= Array.from(this.data.keys());
        return response.send({list })
    }


}

module.exports = LRU